#!/bin/sh

set -ex

export TAR_OPTIONS='--owner root --group root --mode a+rX'
export GZIP_OPTIONS='-9n'
pwd=$(pwd)
dfsg_version="$1"
if [ -z "$dfsg_version" ]
then
	printf 'Usage: %s <version>\n' "$0"
	exit 1
fi
upstream_version="${dfsg_version%+dfsg*}"
cd "$(dirname "$0")/../"
tmpdir=$(mktemp -t -d get-orig-source.XXXXXX)
uscan --noconf --force-download --rename --download-version="$upstream_version" --destdir="$tmpdir"
cd "$tmpdir"
tar -xzf python-qt4_*.orig.tar.gz
rm python-qt4_*.orig.tar.gz

mv PyQt-x11-gpl-* "python-qt4-${dfsg_version}.orig"

# minimized jquery and underscore
rm -rf "python-qt4-${dfsg_version}.orig/doc/html/_static"
rm -f "python-qt4-${dfsg_version}.orig/doc/html/searchindex.js"
rm -f "python-qt4-${dfsg_version}.orig/examples/webkit/fancybrowser/jquery.min.js"
rm -f "python-qt4-${dfsg_version}.orig/examples/webkit/fancybrowser/jquery_rc2.py"
rm -f "python-qt4-${dfsg_version}.orig/examples/webkit/fancybrowser/jquery_rc3.py"

tar -czf "$pwd/python-qt4_${dfsg_version}.orig.tar.gz" "python-qt4-${dfsg_version}.orig"
rm -rf "$tmpdir"
